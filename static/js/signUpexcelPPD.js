$(function(){
	$('#btnextractppd').click(function(){
		
		$.ajax({
			url: '/signUpexcelPPD',
			data: $('form').serialize(),
			type: 'POST',
			success: function(response){
				console.log(response);
				test = JSON.parse(response);
				$('#resultats').empty();
				if(test['error']) $('#resultats').append("Une erreur est survenue vérifiez le format de votre fichier excel");
				if(test['success']) $('#resultats').append("L'opération s'est déroulé correctement");
			},
			error: function(error){
				console.log(error);
				$('#resultats').empty();
			    $('#resultats').append("Une erreur est survenue vérifiez le format de votre fichier excel");
			}
		});
	});
});
