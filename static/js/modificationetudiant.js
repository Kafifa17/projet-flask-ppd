$(function(){
	$('#btnmodifetudiant').click(function(){
		
		$.ajax({
			url: '/modificationetudiant',
			data: $('form').serialize(),
			type: 'POST',
			success: function(response){
				console.log(response);
				test = JSON.parse(response);
				$('#resultats').empty();
				if(test['error']){
					if(test['error'] == "Pas de nom entreprise"){
					$('#resultats').append("Veuillez remplis le champ entreprise avant le champ mission");
					} else {
					$('#resultats').append("Une erreur est survenue vérifiez les champs remplis");
					}
				}
				if(test['success']) $('#resultats').append("L'opération s'est déroulé correctement");
			},
			error: function(error){
				console.log(error);
				$('#resultats').empty();
			   $('#resultats').append("Une erreur est survenue vérifiez les champs remplis");
			}
		});
	});
});
