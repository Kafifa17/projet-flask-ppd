-- MySQL Script generated by MySQL Workbench
-- Mon Feb 12 19:10:44 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Enseignant`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Enseignant` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Enseignant` (
  `idEnseignant` INT NOT NULL AUTO_INCREMENT,
  `nomEnseignant` VARCHAR(45) NULL,
  `mailEnseignant` VARCHAR(45) NULL,
  `idEtudiant` INT NULL,
  PRIMARY KEY (`idEnseignant`),
  INDEX `idEtudiantFK_idx` (`idEtudiant` ASC),
  CONSTRAINT `idEtudiantFK`
    FOREIGN KEY (`idEtudiant`)
    REFERENCES `mydb`.`Etudiant` (`idEtudiant`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `mydb`.`MaitreApprentissage`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`MaitreApprentissage` ;

CREATE TABLE IF NOT EXISTS `mydb`.`MaitreApprentissage` (
  `idMaitreApprentissage` INT NOT NULL AUTO_INCREMENT,
  `nomMaitreApprentissage` VARCHAR(45) NULL,
  `mailMaitreApprentissage` VARCHAR(45) NULL,
  `prenomMaitreApprentissage` VARCHAR(45) NULL,
  PRIMARY KEY (`idMaitreApprentissage`))
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `mydb`.`PPD`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`PPD` ;

CREATE TABLE IF NOT EXISTS `mydb`.`PPD` (
  `idPPD` INT NOT NULL AUTO_INCREMENT,
  `descriptionPPD` VARCHAR(45) NULL,
  PRIMARY KEY (`idPPD`))
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `mydb`.`Mission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Mission` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Mission` (
  `idMission` INT NOT NULL AUTO_INCREMENT,
  `lieuMission` VARCHAR(45) NULL,
  `resumeMission` VARCHAR(255) NULL,
  `codePostalMission` VARCHAR(10) NULL,
  `idEtudiant` INT NULL,
  `nomEntreprise` VARCHAR(255) NULL,
  PRIMARY KEY (`idMission`),
  INDEX `idEtudiantFK_idx` (`idEtudiant` ASC),
  CONSTRAINT `idEtudiantFK2`
    FOREIGN KEY (`idEtudiant`)
    REFERENCES `mydb`.`Etudiant` (`idEtudiant`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `mydb`.`Etudiant`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Etudiant` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Etudiant` (
  `idEtudiant` INT NOT NULL AUTO_INCREMENT,
  `nomEtudiant` VARCHAR(45) NULL,
  `mailEtudiant` VARCHAR(45) NULL,
  `prenomEtudiant` VARCHAR(45) NULL,
  `idTuteurEnseignant` INT NULL,
  `idMA` INT NULL,
  `idPPD` INT NULL,
  `idMission` INT NULL,
  PRIMARY KEY (`idEtudiant`),
  INDEX `idtuteurEnseignantFK_idx` (`idTuteurEnseignant` ASC),
  INDEX `idMAFK_idx` (`idMA` ASC),
  INDEX `idPPDFK_idx` (`idPPD` ASC),
  INDEX `idMissionFK_idx` (`idMission` ASC),
  CONSTRAINT `idtuteurEnseignantFK`
    FOREIGN KEY (`idTuteurEnseignant`)
    REFERENCES `mydb`.`Enseignant` (`idEnseignant`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idMAFK`
    FOREIGN KEY (`idMA`)
    REFERENCES `mydb`.`MaitreApprentissage` (`idMaitreApprentissage`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idPPDFK`
    FOREIGN KEY (`idPPD`)
    REFERENCES `mydb`.`PPD` (`idPPD`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idMissionFK`
    FOREIGN KEY (`idMission`)
    REFERENCES `mydb`.`Mission` (`idMission`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `mydb`.`Intervenant`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Intervenant` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Intervenant` (
  `idIntervenant` INT NOT NULL AUTO_INCREMENT,
  `nomIntervenant` VARCHAR(45) NULL,
  `mailIntervenant` VARCHAR(45) NULL,
  `prenomIntervenant` VARCHAR(45) NULL,
  PRIMARY KEY (`idIntervenant`))
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `mydb`.`Cours`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Cours` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Cours` (
  `idCours` INT NOT NULL AUTO_INCREMENT,
  `intituleCours` VARCHAR(45) NULL,
  `horaireDebut` DATE NULL,
  `horaireFin` DATE NULL,
  `idEnseignant` INT NULL,
  PRIMARY KEY (`idCours`))
ENGINE = InnoDB
AUTO_INCREMENT = 1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
