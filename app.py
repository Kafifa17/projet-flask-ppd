from flask import Flask, render_template, json, request
from flask_mail import Mail, Message
from flaskext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash

mysql = MySQL()
app = Flask(__name__)

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'root'
app.config['MYSQL_DATABASE_DB'] = 'mydb'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'ppd.m2.miage.20172018@gmail.com'
app.config['MAIL_PASSWORD'] = 'AZERTYUI1'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True


mail = Mail(app)

@app.route('/')
def main():
    return render_template('signupexcel.html')


@app.route('/consultationEtudiant')
def consultationEtudiant():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute('SELECT et.idEtudiant as idet, et.nometudiant as nomet, et.prenometudiant as prenomet, et.mailetudiant as mailet, en.nomEnseignant as nomen, ppd.descriptionPPD as descppd, m.nomEntreprise as nomentre, m.resumeMission as resmis FROM etudiant et left join enseignant en on en.idEtudiant = et.idEtudiant left join ppd ppd on ppd.idPPD = et.idPPD left join mission m on m.idEtudiant = et.idEtudiant')
    row_headers=[x[0] for x in cursor.description]
    dataetudiant = cursor.fetchall()
    json_data=[]
    for result in dataetudiant:
        json_data.append(dict(zip(row_headers,result)))
    print(json.dumps(json_data))
    dataetudiant = json.loads(json.dumps(json_data))
    return render_template('consultationEtudiant.html',data=dataetudiant)

@app.route('/planningconversion')
def planningConversion():
    return render_template('planningconversion.html')
	
@app.route('/showSignUpExcel')
def showSignUpExcel():
    return render_template('signupexcel.html')

@app.route('/showSignUpExcelEnseignant')
def showSignUpExcelEnseignant():
    return render_template('signupexcelEnseignant.html')

@app.route('/showSignUpExcelMA')
def showSignUpExcelMA():
    return render_template('signupexcelMA.html')

@app.route('/showSignUpExcelPPD')
def showSignUpExcelPPD():
    return render_template('signupexcelPPD.html')

@app.route('/showmodificationetudiant')
def showmodificationetudiant():
    idetudiant = request.args.get('id')
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute('Select * from etudiant where idEtudiant = %s',idetudiant)
    row_headers=[x[0] for x in cursor.description]
    dataetudiant = cursor.fetchall()
    json_data=[]
    for result in dataetudiant:
        json_data.append(dict(zip(row_headers,result)))
    print(json.dumps(json_data))
    dataetudiant = json.loads(json.dumps(json_data))
	
    cursor.execute('Select * from ppd')
    row_headers=[x[0] for x in cursor.description]
    datappd = cursor.fetchall()
    json_data=[]
    for result in datappd:
        json_data.append(dict(zip(row_headers,result)))
    print(json.dumps(json_data))
    datappd = json.loads(json.dumps(json_data))
	
    cursor.execute('Select * from mission where idEtudiant = %s',idetudiant)
    row_headers=[x[0] for x in cursor.description]
    datamission = cursor.fetchall()
    json_data=[]
    for result in datamission:
        json_data.append(dict(zip(row_headers,result)))
    print(json.dumps(json_data))
    datamission = json.loads(json.dumps(json_data))
	
    cursor.close() 
    conn.close()
    if datamission:
        return render_template('modificationEtudiant.html',data=dataetudiant,datappd=datappd,datamission=datamission)
    else:
        return render_template('modificationEtudiant.html',data=dataetudiant,datappd=datappd)

@app.route('/showsendmail')
def showsendmail():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute('Select nometudiant,idEtudiant from etudiant')
    row_headers=[x[0] for x in cursor.description]
    dataetudiant = cursor.fetchall()
    json_data=[]
    for result in dataetudiant:
        json_data.append(dict(zip(row_headers,result)))
    print(json.dumps(json_data))
    dataetudiant = json.loads(json.dumps(json_data))
	
    cursor.execute('Select nomEnseignant,idEnseignant from enseignant')
    row_headers=[x[0] for x in cursor.description]
    dataenseignant = cursor.fetchall()
    json_data_enseignant=[]
    for result in dataenseignant:
        json_data_enseignant.append(dict(zip(row_headers,result)))
    print(json.dumps(json_data_enseignant))
    dataenseignant = json.loads(json.dumps(json_data_enseignant))
	
    cursor.close() 
    conn.close()
    return render_template('sendmail.html',data=dataetudiant,dataenseignant=dataenseignant)


		
@app.route('/signUpexcel',methods=['POST','GET'])
def signUpexcel():
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        _extraction = request.form['extraction']
    
           
        if _extraction:
            test=_extraction
            jsonStr = json.loads(test)
           # _hashed_password = generate_password_hash(_password)
            test=_extraction
            for item in jsonStr:
               cursor.execute('Insert into etudiant(nometudiant,mailetudiant,prenometudiant) values (%s,%s,%s)',(item['nometudiant'],item['mailetudiant'],item['prenometudiant'],))
            data = cursor.fetchall()

            if len(data) is 0:
                conn.commit()
                return json.dumps({'success':jsonStr[0]})
            else:
                return json.dumps({'error':str(data[0])})
				
        else:
            return json.dumps({'html':'<span>Enter the required fields</span>'})

    except Exception as e:
        return json.dumps({'error':str(e)})
    finally:
            cursor.close() 
            conn.close()

@app.route('/signUpexcelEnseignant',methods=['POST','GET'])
def signUpexcelEnseignant():
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        extractionenseignant = request.form['extractionenseignant']
        print(extractionenseignant)
        
         # All Good, let's call MySQL
        if extractionenseignant:
            jsonStrEnseignant = json.loads(extractionenseignant)
           # _hashed_password = generate_password_hash(_password)
         
            for item in jsonStrEnseignant:
               cursor.execute('Insert into enseignant(nomEnseignant,mailEnseignant) values (%s,%s)',(item['nomenseignant'],item['mailenseignant'],))
            data = cursor.fetchall()

            if len(data) is 0:
                conn.commit()
                return json.dumps({'success':str(jsonStrEnseignant[0]['nomenseignant'])})
            else:
                return json.dumps({'error':str(data[0])})
        else:
            return json.dumps({'html':'<span>Enter the required fields</span>'})

    except Exception as e:
        return json.dumps({'error':str(e)})
    finally:
            cursor.close() 
            conn.close()

@app.route('/signUpexcelMA',methods=['POST','GET'])
def signUpexcelMA():
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        extractionma = request.form['extractionma']
        print(extractionma)
        
         # All Good, let's call MySQL
        if extractionma:
            jsonStrma = json.loads(extractionma)
           # _hashed_password = generate_password_hash(_password)
         
            for item in jsonStrma:
               cursor.execute('Insert into maitreapprentissage(nomMaitreApprentissage,prenomMaitreApprentissage,mailMaitreApprentissage) values (%s,%s,%s)',(item['nomma'],item['prenomma'],item['mailma'],))
            data = cursor.fetchall()

            if len(data) is 0:
                conn.commit()
                return json.dumps({'success':jsonStrma[0]})
                
            else:
                return json.dumps({'error':str(data[0])})
        else:
            return json.dumps({'html':'<span>Enter the required fields</span>'})

    except Exception as e:
        return json.dumps({'error':str(e)})
    finally:
            cursor.close() 
            conn.close()

@app.route('/signUpexcelPPD',methods=['POST','GET'])
def signUpexcelPPD():
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        extractionppd = request.form['extractionppd']
        print(extractionppd)
        
         # All Good, let's call MySQL
        if extractionppd:
            jsonStrppd = json.loads(extractionppd)
           # _hashed_password = generate_password_hash(_password)
         
            for item in jsonStrppd:
               cursor.execute('Insert into ppd(descriptionPPD) values (%s)',(item['PPD'],))
            data = cursor.fetchall()

            if len(data) is 0:
                conn.commit()
                return json.dumps({'success': str(jsonStrppd[0]['PPD'])})
            else:
                return json.dumps({'error':str(data[0])})
        else:
            return json.dumps({'html':'<span>Enter the required fields</span>'})

    except Exception as e:
        return json.dumps({'error':str(e)})
    finally:
            cursor.close() 
            conn.close()

@app.route('/modificationetudiant',methods=['POST','GET'])
def modificationetudiant():
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        nometudiant = request.form['nometudiant']
        prenometudiant = request.form['prenometudiant']
        mailetudiant = request.form['mailetudiant']
        idppd = request.form['ppdid']
        idetudiant = request.form['idetudiant']
        resumemission = request.form['resumemission']
        nomentreprise = request.form['entreprise']
        
        
         # All Good, let's call MySQL
        if nometudiant and prenometudiant:
           # _hashed_password = generate_password_hash(_password)
         
            if idppd:
                cursor.execute('UPDATE etudiant et SET et.nometudiant=%s,et.prenometudiant=%s,mailetudiant=%s,idPPD=%s where et.idEtudiant = %s',(nometudiant,prenometudiant,mailetudiant,idppd,idetudiant,))
                data = cursor.fetchall()
                print(cursor._last_executed)
            else:
                cursor.execute('UPDATE etudiant et SET et.nometudiant=%s,et.prenometudiant=%s,mailetudiant=%s,idPPD=NULL where et.idEtudiant = %s',(nometudiant,prenometudiant,mailetudiant,idetudiant,))
                data = cursor.fetchall()
            if len(data) is 0:
                conn.commit()
            else:
                return json.dumps({'error':str(data[0])})
		#ajout/modification mission
        cursor.execute('Select * from mission where idEtudiant=%s',(idetudiant,))
        datamission = cursor.fetchall()
        print(datamission)
        if len(datamission) is 0:
            if nomentreprise:
               cursor.execute('Insert into mission(nomEntreprise,resumeMission,idEtudiant) values (%s,%s,%s)',(nomentreprise,resumemission,idetudiant,))
               conn.commit()
               print(cursor._last_executed)
            elif not nomentreprise and resumemission:
               return json.dumps({'error':'Pas de nom entreprise'})
        else:
              cursor.execute('Update mission set resumeMission=%s,nomEntreprise=%s where idetudiant=%s',(resumemission,nomentreprise,idetudiant,))
              conn.commit()
        return json.dumps({'success': str(datamission)})
    except Exception as e:
        return json.dumps({'error':str(e)})
    finally:
            cursor.close() 
            conn.close()


@app.route('/sendmail',methods=['POST','GET'])
def sendmail():
    try:
        etudiantid = request.form['etudiantid']
        enseignantid = request.form['enseignantid']
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute('UPDATE etudiant et, enseignant en SET et.idTuteurEnseignant = %s, en.idEtudiant = %s Where et.idEtudiant = %s and en.idEnseignant = %s',(enseignantid, etudiantid, etudiantid, enseignantid,))
        datatest = cursor.fetchall()
        print(cursor._last_executed)
        if len(datatest) is 0:
                conn.commit()
        else:
                return json.dumps({'error':str(datatest[0])})
        cursor.execute('Select * from mydb.etudiant et, mydb.enseignant en where et.idEtudiant = %s and en.idEnseignant = %s and et.idTuteurEnseignant = en.idEnseignant and en.idEtudiant = et.idEtudiant',(etudiantid, enseignantid,))
        row_headers=[x[0] for x in cursor.description]
        data = cursor.fetchall()
        json_data=[]
        for result in data:
           json_data.append(dict(zip(row_headers,result)))
        data = json.loads(json.dumps(json_data))
        
        msg = Message(
               'Send Mail tutorial!',
                sender='ppd.m2.miage.20172018@gmail.com',
                body="Bonjour Monsieur " + str(data[0]['nometudiant']) + " votre tuteur enseignant est monsieur " + str(data[0]['nomEnseignant']) +""
        )
        msg.recipients = ["ppd.m2.miage.20172018@gmail.com"]
        for mailetudiant in data:
           msg.add_recipient(mailetudiant['mailetudiant'])
        for mailenseignant in data:
           msg.add_recipient(mailetudiant['mailEnseignant'])
        mail.send(msg)
        cursor.close() 
        conn.close()
       
    except Exception as e:
        return json.dumps({'error':str(e)})
    return json.dumps({'success':'Envoie de mail'})
 
 
if __name__ == "__main__":
    app.debug = True
    app.run(port=5002)
